package com.micro.fast.upms.controller;

import com.micro.fast.boot.starter.common.response.BaseConst;
import com.micro.fast.boot.starter.common.response.ServerResponse;
import com.micro.fast.upms.pojo.UpmsUserPermission;
import com.micro.fast.upms.service.UpmsUserPermissionService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.hibernate.validator.constraints.NotBlank;
import org.hibernate.validator.constraints.NotEmpty;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.BindException;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

/**
*
* @author lsy
*/
@Api("upmsUserPermission")
@RestController
@RequestMapping("/upmsUserPermission")
public class UpmsUserPermissionController {

  @Autowired
  private UpmsUserPermissionService<UpmsUserPermission,Integer> upmsUserPermissionService;

  @ApiOperation("添加信息")
  @PostMapping
  public ServerResponse addUpmsUserPermission(@Valid UpmsUserPermission upmsUserPermission) throws BindException {
    return  upmsUserPermissionService.add(upmsUserPermission);
  }

  @ApiOperation("根据id查询详细信息")
  @GetMapping("/{id}")
  public ServerResponse getUpmsUserPermissionById(
  @PathVariable(value = "id") @NotBlank(message = BaseConst.BASEMSG_PREFIX+"请传入id") String id){
    return upmsUserPermissionService.getById(Integer.valueOf(id));
  }

  @ApiOperation("根据条件分页查询")
  @GetMapping
  public ServerResponse getUpmsUserPermissionByCondition(UpmsUserPermission upmsUserPermission,
                                             @RequestParam(defaultValue = "1",required = false) int pageNum,
                                             @RequestParam(defaultValue = "10",required = false)int pageSize,
                                             @RequestParam(required = false) String orderBy){
    return upmsUserPermissionService.getByCondition(upmsUserPermission,pageNum,pageSize,orderBy);
  }

  @ApiOperation("修改信息")
  @PutMapping("/{id}")
  public ServerResponse updateUpmsUserPermission(UpmsUserPermission upmsUserPermission,
                                     @NotBlank(message = BaseConst.BASEMSG_PREFIX+"请传入id") String id){
    upmsUserPermission.setUserPermissionId(Integer.valueOf(id));
    return upmsUserPermissionService.update(upmsUserPermission);
  }

  @ApiOperation("根据id删除，传入数组")
  @DeleteMapping
  public ServerResponse deleteUpmsUserPermission(@NotEmpty(message = BaseConst.BASEMSG_PREFIX+"请传入ids") List<Integer> ids){
    return upmsUserPermissionService.deleteByIds(ids);
  }
}
