package com.micro.fast.upms.service.impl;

import com.micro.fast.common.service.impl.SsmServiceImpl;
import com.micro.fast.upms.dao.UpmsRoleMapper;
import com.micro.fast.upms.pojo.UpmsRole;
import com.micro.fast.upms.service.UpmsRoleService;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
* @author lsy
*/
@Service
public class UpmsRoleServiceImpl  extends SsmServiceImpl<UpmsRole,Integer,UpmsRoleMapper>
implements UpmsRoleService<UpmsRole,Integer>,InitializingBean {

  @Autowired
  private UpmsRoleMapper mapper;

  /**
  * 在这个bean初始化完成后覆盖父类的mapper
  */
  @Override
  public void afterPropertiesSet() throws Exception {
    super.setMapper(this.mapper);
  }
}
